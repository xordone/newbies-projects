class Rock():
    def __init__(self) -> None:
        self.name = 'rock'
        self.danger = 'paper'


class Paper():
    def __init__(self) -> None:
        self.name = 'paper'
        self.danger = 'scissors'


class Scissors():
    def __init__(self) -> None:
        self.name = 'scissors'
        self.danger = 'rock'


def check(a: object, b: object) -> bool:
    if a.name == b.danger:
        return a.name
    elif a.name == b.name:
        return 'Ничья'
    return b.name


a = Rock()
b = Rock()

print(check(a, b))

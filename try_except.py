def try_except(fn):
    def wrapped(*args, **kwargs):
        try:
            return fn(*args, **kwargs)
        except Exception as e:
            print('Exception: {0}\n{1}\n{2}'.format(e.__class__, e, e.__doc__))

    return wrapped

# By Lewis Crossman
